package com.inec.server.model.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.inec.server.model.bean.UsuarioFiscalizador;
import com.inec.shared.BeanParametro;
import com.inec.shared.UnknownException;

public class DaoUsuarioFiscalizador {
	private PersistenceManager pm;

	public DaoUsuarioFiscalizador(PersistenceManager pm) {
		this.pm = pm;
	}

	public boolean mantenimiento(BeanParametro parametro)
			throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.mantenimiento(parametro);
	}
	public boolean mantenimiento(List<BeanParametro> listParametros)
			throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.mantenimiento(listParametros);
	}
	
	public Object getBean(String id) throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.getBean(UsuarioFiscalizador.class, id);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public UsuarioFiscalizador getBeanByDni(String dniFiscalizador) throws UnknownException {
		Query query = pm.newQuery(UsuarioFiscalizador.class);				
		String declareParameters="String paramDniUsuario";
		String filter="dniFiscalizador==paramDniUsuario";
		try{
		List<UsuarioFiscalizador> lista=new ArrayList<UsuarioFiscalizador>();
		Querys consulta=new Querys();
		List beanObject=new ArrayList();
		beanObject.add(dniFiscalizador);
		lista.addAll((List<UsuarioFiscalizador>)consulta.sendQuery(1,declareParameters , filter, null, beanObject, query));
		if(!lista.isEmpty()){
			return lista.get(0);
		}
		return null;
		}catch(Exception ex){			
			throw new UnknownException(ex.getMessage());
		}finally{
			query.closeAll();
		}	
	}	
	@SuppressWarnings("unchecked")
	public Collection<UsuarioFiscalizador> getListarBean() throws UnknownException {
		Querys query = new Querys(this.pm);
		Collection<UsuarioFiscalizador> lista = (Collection<UsuarioFiscalizador>) query
				.getListaBean(UsuarioFiscalizador.class);
		return lista;
	}	
}
