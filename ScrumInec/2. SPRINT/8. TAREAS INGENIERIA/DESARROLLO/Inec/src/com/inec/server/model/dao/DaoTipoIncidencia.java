package com.inec.server.model.dao;

import java.util.List;

import javax.jdo.PersistenceManager;

import com.inec.server.model.bean.TipoIncidencia;
import com.inec.shared.BeanParametro;
import com.inec.shared.UnknownException;

public class DaoTipoIncidencia {
	private PersistenceManager pm;

	public DaoTipoIncidencia(PersistenceManager pm) {
		this.pm = pm;
	}

	public boolean mantenimiento(BeanParametro parametro)
			throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.mantenimiento(parametro);
	}
	public boolean mantenimiento(List<BeanParametro> listParametros)
			throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.mantenimiento(listParametros);
	}
	
	public Object getBean(String id) throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.getBean(TipoIncidencia.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoIncidencia> getListarBean() throws UnknownException {
		Querys query = new Querys(this.pm);
		List<TipoIncidencia> lista = (List<TipoIncidencia>) query
				.getListaBean(TipoIncidencia.class);
		return lista;
	}	
}
