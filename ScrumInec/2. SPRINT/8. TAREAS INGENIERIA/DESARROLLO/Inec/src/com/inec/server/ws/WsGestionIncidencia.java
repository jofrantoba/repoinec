package com.inec.server.ws;

import java.util.List;

import javax.inject.Named;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.inec.server.model.process.GestionIncidencia;
import com.inec.shared.ReturnValue;
import com.inec.shared.UnknownException;

@Api(name = "gestionIncidencia", namespace = @ApiNamespace(ownerDomain = "inec.com", ownerName = "inec.com", packagePath = "server.ws"))
public class WsGestionIncidencia {

	@ApiMethod(name = "listColoniaInteres",path="listColoniaInteres")
	public ReturnValue listColoniaInteres(@Named("wsCodeColonia")String wsCodeColonia) {
		ReturnValue valorRetorno = new ReturnValue();
		try {
			valorRetorno.setNameClass(List.class);
			valorRetorno.setValueReturn(GestionIncidencia.registrarIncidencia(wsCodeColonia));
		} catch (UnknownException e) {
			valorRetorno.setNameClass(UnknownException.class);
			valorRetorno.setValueReturn(e.getLocalizedMessage());
		}
		return valorRetorno;
	}
}
