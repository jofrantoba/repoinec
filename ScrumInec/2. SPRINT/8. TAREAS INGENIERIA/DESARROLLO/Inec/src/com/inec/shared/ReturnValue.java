package com.inec.shared;

import java.io.Serializable;
import java.util.List;

import com.inec.server.model.bean.PartidoPolitico;
import com.inec.server.model.bean.TipoIncidencia;

public class ReturnValue implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nameClass;
	private Object valueReturn;
	private TipoIncidencia returnTipoIncidencia;
	private List<TipoIncidencia> returnListTipoIncidencia;
	private PartidoPolitico returnPartidoPolitico;
	private List<PartidoPolitico> returnListPartidoPolitico;
	
	public String getNameClass() {
		return nameClass;
	}
	public void setNameClass(Class<?> ObjectNameClass) {
		this.nameClass = ObjectNameClass.getSimpleName();
	}
	public Object getValueReturn() {
		return valueReturn;
	}
	public void setValueReturn(Object valueReturn) {
		this.valueReturn = valueReturn;
	}
	public TipoIncidencia getReturnTipoIncidencia() {
		return returnTipoIncidencia;
	}
	public void setReturnTipoIncidencia(TipoIncidencia returnTipoIncidencia) {
		this.returnTipoIncidencia = returnTipoIncidencia;
	}
	public List<TipoIncidencia> getReturnListTipoIncidencia() {
		return returnListTipoIncidencia;
	}
	public void setReturnListTipoIncidencia(List<TipoIncidencia> returnListTipoIncidencia) {
		this.returnListTipoIncidencia = returnListTipoIncidencia;
	}
	public PartidoPolitico getReturnPartidoPolitico() {
		return returnPartidoPolitico;
	}
	public void setReturnPartidoPolitico(PartidoPolitico returnPartidoPolitico) {
		this.returnPartidoPolitico = returnPartidoPolitico;
	}
	public List<PartidoPolitico> getReturnListPartidoPolitico() {
		return returnListPartidoPolitico;
	}
	public void setReturnListPartidoPolitico(List<PartidoPolitico> returnListPartidoPolitico) {
		this.returnListPartidoPolitico = returnListPartidoPolitico;
	}		
}
