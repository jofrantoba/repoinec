package com.inec.server.model.dao;

import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;

import com.inec.server.model.bean.Ubigeo;
import com.inec.shared.BeanParametro;
import com.inec.shared.UnknownException;

public class DaoUbigeo {
	private PersistenceManager pm;

	public DaoUbigeo(PersistenceManager pm) {
		this.pm = pm;
	}

	public boolean mantenimiento(BeanParametro parametro)
			throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.mantenimiento(parametro);
	}
	public boolean mantenimiento(List<BeanParametro> listParametros)
			throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.mantenimiento(listParametros);
	}
	
	public Object getBean(String id) throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.getBean(Ubigeo.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ubigeo> getListarBean() throws UnknownException {
		Querys query = new Querys(this.pm);
		Collection<Ubigeo> lista = (Collection<Ubigeo>) query
				.getListaBean(Ubigeo.class);
		return lista;
	}	
}
