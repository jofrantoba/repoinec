package com.inec.uicapturarfoto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.inec.uiregistrarincidencia.UiRegistrarIncidencia;
import com.inec.utils.PreferenciasSession;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import inec.com.R;

public class UiCapturarFoto extends Activity  implements InterCapturarFoto, View.OnClickListener  {
    public static final int CAMERA_REQUEST = 1888;
    public ImageButton btnAceptar;
    public ImageButton btnAtras;
    public ImageButton btnTomarFoto;
    public ImageView imgFoto;
    public static boolean fotoTomada=false;
    public static boolean fotoVisible=false;
    public static boolean ubicacionObtenida=false;
    public static boolean envioRealizado=false;
    public SharedPreferences mPrefSesion;
    public Bitmap bm=null;
    public String resource;
    public static List<String> listResources= new ArrayList<>();
    public static File resourcesFile=null;
    public static List<File> listFile=new ArrayList<>();
    public static Integer contador=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_capturar_foto);
        initComponents();
        initListener();
    }

    private void initComponents(){
        mPrefSesion = getSharedPreferences(PreferenciasSession.NAME_PREFERENCE, Context.MODE_PRIVATE);
        btnAceptar = (ImageButton) findViewById(R.id.btnAceptar);
        btnAtras = (ImageButton) findViewById(R.id.btnQuitar);
        btnTomarFoto = (ImageButton) findViewById(R.id.btnAgregarFoto);
        imgFoto = (ImageView) findViewById(R.id.img_fotografia);
    }

    public void initComponentsGeo(){

    }

    private void initListener(){
        btnAceptar.setOnClickListener(this);
        btnAtras.setOnClickListener(this);
        btnTomarFoto.setOnClickListener(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (!fotoTomada) {
            if (!fotoVisible) {
                TomarFoto();
            } else {
                VerImagen();
            }
        } else {
            VerImagen();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAceptar:
                envioRealizado=true;
                addImagen();
                UploadImage();
                Intent intent = new Intent(this, UiRegistrarIncidencia.class);
                startActivity(intent);
                return;
            case R.id.btnAgregarFoto:
                TomarFoto();
                addImagen();
                return;
            case R.id.btnQuitar:
                TomarFoto();
                return;
            default:
                return;
        }
    }

    public void TomarFoto(){
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        resource="image_"+new java.util.Date().getTime()+".jpg";
//        resource="inec_00"+contador+".jpg";
        File image = getFileImage();

        Uri uriSavedImage = Uri.fromFile(image);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    public File getFileImage(){
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "InecImages");
        if (!imagesFolder.exists()) imagesFolder.mkdirs();
        File image = new File(imagesFolder, resource);
        resourcesFile=image;
        return image;
    }

    public void VerImagen(){
        final BitmapFactory.Options options = new BitmapFactory.Options();
        ExifInterface exif=null;
        File image=getFileImage();
        long length = image.length();
        length = length/1024;
        if(length>2000) {
            options.inSampleSize = 6;
        }else if(length>1000) {
            options.inSampleSize = 4;
        }else if(length>700) {
            options.inSampleSize = 3;
        }else if(length>250) {
            options.inSampleSize = 2;
        }
        bm=BitmapFactory.decodeFile(image.getPath(),options);
        int orientation=0;
        int angle=0;

        try {
            exif = new ExifInterface(image.getPath());
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        } catch (IOException e) {
            e.printStackTrace();
        }

        switch(orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                angle= 90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                angle= 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                angle=270;
                break;
        }
        Matrix mat = new Matrix();
        mat.postRotate(angle);
        bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), mat, true);
        imgFoto.setImageBitmap(bm);
        fotoVisible=true;
    }

    @Override
    protected void onDestroy() {
        if(bm!=null) {
            bm.recycle();
            bm=null;
        }
        super.onDestroy();
    }


    public void UploadImage(){

    }

    @Override
    public void capturaFoto() {

    }

    @Override
    public File getFileResources() {
        return null;
    }


    public void addImagen(){
        listFile.add(resourcesFile);
        Log.d("LISTAAAA ", listFile.toString());
        listResources.add(resource);
        contador++;
    }


}
