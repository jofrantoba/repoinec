package com.inec.uicapturarfoto;

import java.io.File;

/**
 * Created by root on 10/10/16.
 */

public interface InterCapturarFoto {

    public void capturaFoto();
    public File getFileResources();
}
