package com.inec.uihomeapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;

import com.android.volley.toolbox.NetworkImageView;
import com.inec.uicapturarfoto.UiCapturarFotoImpl;
import com.inec.utils.PreferenciasSession;

import java.io.IOException;
import java.text.Normalizer;
import java.util.List;
import java.util.Locale;

import inec.com.R;

public class UiHomeApp extends Activity implements View.OnClickListener {
    public String mCodeDestino;
    public Double mDistanciaBusqueda;
    public boolean mDesdeGPS;
    public Double mLatitud;
    public Double mLongitud;
    public SharedPreferences mPrefSesion;
    public LinearLayout layoutConquistar;
    public ImageButton btnVerMapa;
    public ImageButton btnValorar;
    public NetworkImageView imgDestino;
    public ListView lvValoracion;
    public boolean mDesdeDestino;
    public Double mLatitudDestino;
    public Double mLongitudDestino;
    public Double mRadioDestino;
    public ScrollView svPrincipal;
    public static Boolean llamarNuevamente;
//    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
//    private UiDestinoValoracionListAdapter listAdapter;

    public String nombrePaisActual;
    public String nombreRegionActual;
    public ProgressDialog pdEspera;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_home_app);
        initComponents();
        initListener();
        llamarNuevamente = false;

    }

    public void initComponents() {
        mPrefSesion = getSharedPreferences(PreferenciasSession.NAME_PREFERENCE, Context.MODE_PRIVATE);
//        mCodeDestino = getIntent().getExtras().getString("CodeDestino", "");
//        mDesdeDestino = getIntent().getExtras().getBoolean("DesdeDestino", false);
//        pdEspera = null;
//        updateDatosPosicion();
        layoutConquistar = (LinearLayout) findViewById(R.id.layoutConquitarDestino);
        imgDestino = (NetworkImageView) findViewById(R.id.imgDestino);
        lvValoracion = (ListView) findViewById(R.id.listaValoracionDestino);
        svPrincipal = (ScrollView) findViewById(R.id.svPrincipal);
    }

    public void initListener() {
        layoutConquistar.setOnClickListener(this);
//        btnVerMapa.setOnClickListener(this);
//        btnValorar.setOnClickListener(this);
        imgDestino.setOnClickListener(this);
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public void verMensajeError(String mensaje, final Boolean irOpcionAnterior) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(mensaje).setTitle("Mensaje");
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.textButtonAceptar, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (irOpcionAnterior) {
                    onBackPressed();
                }
            }
        });
        builder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layoutConquitarDestino:
                ConquistarDestino();
                break;
            case R.id.imgDestino:
                LlamarObtenerGaleriaDestino();
                break;
            default:
                break;
        }
    }

    public void LlamarObtenerGaleriaDestino() {

    }

    public void ConquistarDestino() {
        UiCapturarFotoImpl.ubicacionObtenida = false;
        UiCapturarFotoImpl.envioRealizado = false;
        UiCapturarFotoImpl.fotoTomada = false;
        UiCapturarFotoImpl.fotoVisible = false;
        Intent intent = new Intent(this, UiCapturarFotoImpl.class);
//        intent.putExtra("CodeDestino", mCodeDestino);
//        intent.putExtra("TipoDestino", mDestino.getTipoNegocioDestino());
//        intent.putExtra("NombreDestino", mDestino.getNombre());
//        intent.putExtra("Latitud", mDestino.getLatitud());
//        intent.putExtra("Longitud", mDestino.getLongitud());
        Log.d("ESTAAAAAAAAAAAAAAAAA", "Entra aqui");
        startActivity(intent);
    }

    public void updateDatosPosicion() {
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(mLatitud, mLongitud, 1);
        } catch (IOException e) {
            Log.d("OJO", "ERROR OBTENIENDO UBICACION->" + e.getMessage());
        }

        if (addresses != null) {
            if (addresses.size() > 0) {
                if (addresses.get(0).getCountryName() != null) {
                    nombrePaisActual = cleanStringUpper(addresses.get(0).getCountryName());
                } else {
                    nombrePaisActual = "";
                }
                if (addresses.get(0).getAdminArea() != null) {
                    nombreRegionActual = cleanStringUpper(addresses.get(0).getAdminArea());
                } else {
                    nombreRegionActual = "";
                }
            } else {
                nombrePaisActual = "";
                nombreRegionActual = "";
            }
        } else {
            nombrePaisActual = "";
            nombreRegionActual = "";
        }
    }

    public String cleanStringUpper(String texto) {
        texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
        texto = texto.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return texto.toUpperCase();
    }
}
