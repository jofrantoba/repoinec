package gob.inec.server.bean;

import java.io.Serializable;

public class Fiscalizador implements Serializable{
	private String dniFiscalizador;
	private String codeFiscalizador;
	private String nombreFiscalizador;
	private String apellidoFiscalizador;
	private String telefono;
	private String correoPersonal;
	private String correoCorporativo;
	private String estado;
	private String clave;
	private Zona beanZona;
	

}
