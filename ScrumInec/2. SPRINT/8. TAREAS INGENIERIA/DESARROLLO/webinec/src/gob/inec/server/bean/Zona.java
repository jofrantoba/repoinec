package gob.inec.server.bean;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class Zona implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3908789363293365052L;
	
	@PrimaryKey
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String idZona;
	@Persistent
	private String CodeZona;
	@Persistent
	private String nombreZona;
	@Persistent
	private Ubigeo beanUbigeo;
	@Persistent
	private long version;
	@NotPersistent
	private String operacion;
	
	public String getIdZona() {
		return idZona;
	}
	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}
	public String getCodeZona() {
		return CodeZona;
	}
	public void setCodeZona(String codeZona) {
		CodeZona = codeZona;
	}
	public String getNombreZona() {
		return nombreZona;
	}
	public void setNombreZona(String nombreZona) {
		this.nombreZona = nombreZona;
	}
	public Ubigeo getBeanUbigeo() {
		return beanUbigeo;
	}
	public void setBeanUbigeo(Ubigeo beanUbigeo) {
		this.beanUbigeo = beanUbigeo;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CodeZona == null) ? 0 : CodeZona.hashCode());
		result = prime * result + ((idZona == null) ? 0 : idZona.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Zona other = (Zona) obj;
		if (CodeZona == null) {
			if (other.CodeZona != null)
				return false;
		} else if (!CodeZona.equals(other.CodeZona))
			return false;
		if (idZona == null) {
			if (other.idZona != null)
				return false;
		} else if (!idZona.equals(other.idZona))
			return false;
		return true;
	}
	
	
}
