package gob.inec.server.bean;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class Ubigeo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8921193996927222680L;
	
	@PrimaryKey
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String idUbigeo;
	@Persistent
	private String CodeUbigeo;
	@Persistent
	private String departamento;
	@Persistent
	private String provincia;
	@Persistent
	private String distrito;
	@Persistent
	private long version;
	@NotPersistent
	private String operacion;
	
	public String getIdUbigeo() {
		return idUbigeo;
	}
	public void setIdUbigeo(String idUbigeo) {
		this.idUbigeo = idUbigeo;
	}
	public void setCodeUbigeo(String codeUbigeo) {
		CodeUbigeo = codeUbigeo;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getCodeUbigeo() {
		return CodeUbigeo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CodeUbigeo == null) ? 0 : CodeUbigeo.hashCode());
		result = prime * result + ((idUbigeo == null) ? 0 : idUbigeo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ubigeo other = (Ubigeo) obj;
		if (CodeUbigeo == null) {
			if (other.CodeUbigeo != null)
				return false;
		} else if (!CodeUbigeo.equals(other.CodeUbigeo))
			return false;
		if (idUbigeo == null) {
			if (other.idUbigeo != null)
				return false;
		} else if (!idUbigeo.equals(other.idUbigeo))
			return false;
		return true;
	}
	
	
	

}
